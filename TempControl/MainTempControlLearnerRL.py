# Temp control learner
# Based on Q-Learning but with continuous action space

import numpy as np
import ReinforceLearner
import TempControlGymEnv

RUN_AND_SHOW_DONT_LEARN = False
SERIALIZE_BEST_TABLE = False

EPISODES = 5000
RENDER_EVERY = EPISODES
RENDER_FIRST_EPISODE = True
STATUS_EVERY = 200
SERIALIZE_TABLE_EVERY = 1000

# q-Table file name
doLoadTable = True
qTableFileStr = './tempData/table.npy'

# Model
env = TempControlGymEnv.TempControlEnv()

# Observation Space: states in this environment
OS_MAX = np.array(env.getObervationSpaceMax())
OS_MIN = np.array(env.getObervationSpaceMin())
OBSERVATION_SPACE_DIM = env.getObervationSpaceDim()
OBSERVATION_SPACE_LEN = 20
discret_OS_size_array = [OBSERVATION_SPACE_LEN] * OBSERVATION_SPACE_DIM # = array mit (20, 20)
discrete_os_step_width_array = (OS_MAX - OS_MIN) / discret_OS_size_array

# Action Space: space of possible actions
ACTION_SPACE_DIM = env.getActionSpaceDim()
ACTION_SPACE_LEN = env.getActionSpaceLen()

# Exploration settings
# Epsilon: Wieviel Zufall bei Auswahl der "action" im Spiel sein soll
epsilon = 0.8 # between 0.0 and 1.0. Start value not a constant, going to be decayed
START_EPSILON_DECAYING = 1
END_EPSILON_DECAYING = 1000
epsilon_decay_value = epsilon / (END_EPSILON_DECAYING - START_EPSILON_DECAYING)
END_EPSILON_DECAYING = EPISODES // 2 # "//" = floor division operator

if RUN_AND_SHOW_DONT_LEARN:
    doLoadTable = True
    RENDER_EVERY = EPISODES
    RENDER_FIRST_EPISODE = True
    epsilon = 0.0

rf = ReinforceLearner.ReinforceLearner(discret_OS_size_array, ACTION_SPACE_LEN)

# load table
if not doLoadTable:
    rf.reset("")
else:
    rf.reset(qTableFileStr)

def get_discrete_state(state):
    npState = np.array(state)
    npOSMin = np.array(env.getObervationSpaceMin())
    discrete_state = (npState - npOSMin) / discrete_os_step_width_array
    return tuple(discrete_state.astype(np.int))


# Initialize variables to track rewards
reward_list = []
mean_reward_list = []
bestScoreLastSerialization = -200

for episode in range(EPISODES):

    render = False
    if episode % RENDER_EVERY == 0:
        render = True
    if episode == 0 and not RENDER_FIRST_EPISODE:
        render = False
    if episode > (EPISODES - 2):
        render = True

    current_discrete_state = get_discrete_state(env.reset()) # reset returns the initial state
    # print(np.argmax(q_table[current_state]))

    # Start new episode --------------------------------------------------------------------
    done = False
    tot_rewards = 0
    while not done:
        # State model:
        # current_state ---- action ----> new_state
        # current_q                       new_q_values

        action = rf.getAction(current_discrete_state, epsilon)

        new_state_raw, reward, done, goalReached = env.step(action) # done is true after 200 steps or if goal is reached

        if render:
            env.render()

        # Get some reward if we approach the goal, too:
        maxRewardWithoutReachingGoal = -0.5
        if reward < maxRewardWithoutReachingGoal:
            temperature = new_state_raw[0]
            tempDiff = abs(temperature - env.getGoalTemperature())
            tempMax = env.getObervationSpaceMax()[0]
            tempMin = env.getObervationSpaceMin()[0]
            tempDiffNormalized = tempDiff / (tempMax - tempMin)
            reward = maxRewardWithoutReachingGoal - tempDiffNormalized

        new_state = get_discrete_state(new_state_raw)

        if not RUN_AND_SHOW_DONT_LEARN:
            rf.update(reward, done, current_discrete_state, new_state, goalReached)

        tot_rewards += reward
        current_discrete_state = new_state
    # End of Episode --------------------------------------------------------------------
    reward_list.append(tot_rewards)

    if episode % STATUS_EVERY == 0:
        averageRewardSinceLastStatus = np.mean(reward_list)
        mean_reward_list.append(np.mean(reward_list))
        print('Episode Nr: ' + repr(episode) + ' Average reward: ' + repr(averageRewardSinceLastStatus) + ' Epsilon: ' + repr(epsilon))
        reward_list.clear()

    doSerialize = False
    if SERIALIZE_BEST_TABLE:
        if tot_rewards > bestScoreLastSerialization:
            doSerialize = True
    else:
        if episode + 1 % SERIALIZE_TABLE_EVERY == 0:
            doSerialize = True

    if doSerialize:
        rf.serializeTable(qTableFileStr)
        bestScoreLastSerialization = tot_rewards

    # Decaying is being done every episode if episode number is within decaying range
    if END_EPSILON_DECAYING >= episode >= START_EPSILON_DECAYING:
        epsilon -= epsilon_decay_value

rf.serializeTable(qTableFileStr)

env.close()

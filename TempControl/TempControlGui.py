# flake8: noqa
import PySimpleGUI as sg
import Regelstrecke

class TempControlGui:

    window = {}

    def __init__(self, modelConfig, Tsoll):

        model = Regelstrecke.ModelConfig(0, 0, 0, 0, 0)
        model = modelConfig

        sg.ChangeLookAndFeel('GreenTan')
        # column1 = [[sg.Text('Column 1', background_color='#d3dfda', justification='center', size=(10, 1))],      
        #            [sg.Spin(values=('Spin Box 1', '2', '3'), initial_value='Spin Box 1')],      
        #            [sg.Spin(values=('Spin Box 1', '2', '3'), initial_value='Spin Box 2')],      
        #            [sg.Spin(values=('Spin Box 1', '2', '3'), initial_value='Spin Box 3')]]      
        xPos1 = 25
        layout = [         
            [sg.T('Model')],     
            [sg.T('Wärmeleiter Kapazität(C1):',size=(xPos1, 1)), sg.InputText(model.cp1, key='INPUT_CP1', size=(10, 1))],
            [sg.T('Wärmeübergang (R1):',size=(xPos1, 1)), sg.InputText(model.r1, key='INPUT_R1', size=(10, 1))],
            [sg.T('Kapazität Wärmespeicher (C2):',size=(xPos1, 1)), sg.InputText(model.cp2, key='INPUT_CP2', size=(10, 1))],
            [sg.T('R zu Umgebung (R2):',size=(xPos1, 1)), sg.InputText(model.r2, key='INPUT_R2', size=(10, 1))],
            [sg.T('Vout max:',size=(xPos1, 1)), sg.InputText(model.vmax, key='INPUT_VMAX', size=(10, 1))],
            [sg.Button('ApplyModel')],
            [sg.Text('_'  * 80)], 
            [sg.T('Manual Control')], 
            [sg.T('Voltage (%):',size=(15, 1)), sg.Slider(range=(0, 100), key='SLIDER1', orientation='h', size=(30, 20), default_value=0, enable_events=True)],
            [sg.Text('_'  * 80)], 
            [sg.T('Auto Control')],
            [sg.T('Soll:',size=(15, 1)), sg.InputText(Tsoll, key='IN_SOLL', size=(10, 1))],           
            [sg.T('P:',size=(15, 1)), sg.InputText('0', key='IN_P', size=(10, 1))],           
            [sg.T('I:',size=(15, 1)), sg.InputText('0', key='IN_I', size=(10, 1))],           
            [sg.T('D:',size=(15, 1)), sg.InputText('0', key='IN_D', size=(10, 1))],
            [sg.T('Error:'), sg.T('0',size=(15, 1), key="OUT_TEMP_ERROR")],         
            [sg.T('dError:'), sg.T('0',size=(15, 1), key="OUT_TEMP_DERROR")],         
            [sg.Button('Enable'), sg.T('OFF', key="OUT_AUTOENABLE")],
            [sg.Text('_'  * 80)],
            [sg.T('Simulation')],
            [sg.T('T Ist:'), sg.T('0',size=(15, 1), key="OUT_TEMP_IST")],         
            [sg.T('T Soll:'), sg.T('0',size=(15, 1), key="OUT_TEMP_SOLL")],         
            [sg.T('Fehler Akkum. (abs):'), sg.T('0',size=(15, 1), key="OUT_FEHLER")],
            [sg.T('Step:'), sg.T('0',size=(15,1), key="OUT_STEP")],      
            [sg.Button('Pause'), sg.Button('Reset'), sg.Button('Random')]
        ]

        self.window = sg.Window('Temp Controller', default_element_size=(40, 1)).Layout(layout)


    def __del__(self):        
        self.window.close()

    def read(self, timeOutMs):
        event, values = self.window.read(timeOutMs)
        return event, values

    def set(self, elementString, value):
        return self.window.Element(elementString).Update(value)

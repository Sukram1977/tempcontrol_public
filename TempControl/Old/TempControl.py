import PySimpleGUI as sg
import matplotlib.pyplot as plt
import numpy as np
from collections import deque
import Regelstrecke

# Initializations
filter_init = 0.1
cp_init = 200
resEnv_init = 10
model = Regelstrecke.Strecke(filter_init, cp_init, resEnv_init)

sg.ChangeLookAndFeel('GreenTan')

# column1 = [[sg.Text('Column 1', background_color='#d3dfda', justification='center', size=(10, 1))],      
#            [sg.Spin(values=('Spin Box 1', '2', '3'), initial_value='Spin Box 1')],      
#            [sg.Spin(values=('Spin Box 1', '2', '3'), initial_value='Spin Box 2')],      
#            [sg.Spin(values=('Spin Box 1', '2', '3'), initial_value='Spin Box 3')]]      
layout = [         
    [sg.T('Model')],     
    [sg.T('Wärmeleiter Filter:',size=(15, 1)), sg.InputText(filter_init, key='INPUT_FILT', size=(10, 1))],
    [sg.T('Kapazität Wärmespeicher:',size=(15, 1)), sg.InputText(cp_init, key='INPUT_C', size=(10, 1))],
    [sg.T('R zu Umgebung:',size=(15, 1)), sg.InputText(resEnv_init, key='INPUT_R', size=(10, 1))],    
    [sg.Button('Apply')],
    [sg.Text('_'  * 80)], 
    [sg.T('Manual Control')], 
    [sg.T('Voltage:',size=(15, 1)), sg.Slider(range=(0, 100), key='SLIDER1', orientation='h', size=(30, 20), default_value=0, enable_events=True)],
    [sg.Text('_'  * 80)], 
    [sg.T('Auto Control')],
    [sg.T('Soll:',size=(15, 1)), sg.InputText('0', key='IN_SOLL', size=(10, 1))],           
    [sg.T('P:',size=(15, 1)), sg.InputText('0', key='IN_P', size=(10, 1))],           
    [sg.T('I:',size=(15, 1)), sg.InputText('0', key='IN_I', size=(10, 1))],           
    [sg.T('D:',size=(15, 1)), sg.InputText('0', key='IN_D', size=(10, 1))],
    [sg.Button('Enable'), sg.T('OFF', key="OUT_AUTOENABLE")],
    [sg.Text('_'  * 80)],
    [sg.T('Simulation')],
    [sg.T('Fehler Summe (abs):'), sg.T('0',size=(15, 1), key="OUT_FEHLER")],         
    [sg.Button('Pause'), sg.Button('Reset')] 
]

window = sg.Window('Temp Controller', default_element_size=(40, 1)).Layout(layout)

# plotting
maxBufferSize = 40
plotBuffer = np.zeros(maxBufferSize)
plt.ion() #interactive mode, redraw after each call of draw
ax = plt.gca()
h1, = ax.plot(np.arange(0,maxBufferSize), plotBuffer)

def drawPlot(newvalue):
    # shift all values in array one step right:
    plotBuffer[1:] = plotBuffer[:-1]
    plotBuffer[0] = newvalue
    h1.set_xdata(np.arange(0,maxBufferSize))
    h1.set_ydata(plotBuffer)
    ax.relim()
    ax.autoscale_view()
    plt.draw()
    #plt.pause(1e-17)

pause = False
autoEnable = False
errorSum = 0
errorTempOld = 0
#model.__init__(cp_init, resEnv_init)

while True:
    event, values = window.read(100)
    if event in (None, 'Cancel'):	# if user closes window or clicks cancel
        break
    if event == 'Apply':
        filterParam = values['INPUT_FILT']
        cp = values['INPUT_C']
        Renv = values['INPUT_R']
        model.__init__(filterParam, cp, Renv)
    if event == 'Pause':
        pause = not pause
    if event == 'Reset':
        errorSum = 0
    if event == 'Enable':
        autoEnable = not autoEnable
        if autoEnable:
            window.Element('OUT_AUTOENABLE').Update('ON')
        else:
            window.Element('OUT_AUTOENABLE').Update('OFF')


    voltage  = float(values['SLIDER1'])
    sollTemp = float(values['IN_SOLL'])
    errorTemp = sollTemp - model.getTemperature()

    # PID control
    if autoEnable:
        Kp = float(values['IN_P'])
        Kd = float(values['IN_D'])
        Ki = float(values['IN_I'])
        voltage = Kp * errorTemp + Kd * (errorTemp - errorTempOld)
        errorTempOld = errorTemp
        window.Element('SLIDER1').Update(voltage)
         

    model.setHeaterVoltage(voltage)
    if not pause:
        model.cycle()
        errorSum = errorSum + (abs(errorTemp))
        drawPlot(model.getTemperature())        
        window.Element('OUT_FEHLER').Update(errorSum)
    #print('You entered ', values[0], values['SLIDER1'])

window.close()

#sg.Popup(button, values)
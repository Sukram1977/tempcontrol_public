class Strecke(object):

    # model:
    # Simple model consisting of following heat path:
    # - heatflux between heater and capacity is modelled as a filter 1. order
    # - the heat capacity looses heat to the environment (R_env) 
    # voltage -> capacity1 -> R_heater -> capacity2 -> R_env
    energy = 0       # Energy of main capacity (capacity)
    voltage = 0

    filterParam = 0.1 #(1 = no filter, 0 = infinite response)
    capacity = 1000
    R_env = 10    # Resistance between heat capacity and environment


    voltageFiltered = 0


    def __init__(self, heaterFilter, capacity, R_env):

        self.filterParam = float(heaterFilter)   
        # Wärmekapazität
        self.capacity = float(capacity)
        # Wärmeübergang (Widerstand) zu Umgebung
        self.R_env = float(R_env)
        self.energy = 0       # Energy of main capacity       

    def setHeaterVoltage(self, voltage):
        self.voltage = max(voltage, 0)
    def getTemperature(self):
        return self.energy / self.capacity 

    # update model
    # supposed to be called in a time constant period
    def cycle(self):
        temperature = self.energy / self.capacity 
        self.voltageFiltered = self.voltageFiltered * (1 - self.filterParam) + self.voltage * self.filterParam
        heatFlowEnvironment = (temperature * temperature)  / self.R_env
        self.energy = max(self.energy + self.voltageFiltered * self.voltageFiltered - heatFlowEnvironment, 0)

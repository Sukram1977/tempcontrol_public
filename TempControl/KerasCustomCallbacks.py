from tensorflow import keras
import Chart

class LossAndErrorPrintingCallback(keras.callbacks.Callback):

    maxReward = -9999
    avgReward = 0
    epochCounter = 0
    plotting = False
    isFirstIteration = True
    newLine = False

    def reset(self):
        self.maxReward = -9999
        self.avgReward = 0
        self.epochCounter = 0

    def plotNewLine(self, label):
        self.currentLineLabel = label
        self.newLine = True

    def savePlot(self, fileName='EpocheChart.png'):
        self.chart.save(fileName)

    def doPlot(self, enable):
        self.plotting = enable

    def on_train_batch_end(self, batch, logs=None):
        print(" For batch {}, reward is {:7.2f} ".format(batch, logs["episode_reward"]))

    def on_test_batch_end(self, batch, logs=None):
        print(" For batch {}, reward is {:7.2f} ".format(batch, logs["episode_reward"]))

    def on_epoch_end(self, epoch, logs=None):
        print(" The episode {} reward is {:7.2f}. Max: {:7.2f}, Avg: {:7.2f} ".format(epoch, logs["episode_reward"], self.maxReward, self.avgReward))
        currentReward = logs["episode_reward"]
        if currentReward > self.maxReward:
            self.maxReward = currentReward
        self.epochCounter = self.epochCounter + 1
        self.avgReward = (self.avgReward * (self.epochCounter - 1) + currentReward) / self.epochCounter

        if self.plotting:
            # self.chart.draw(self.currentReward, self.avgReward)
            if self.isFirstIteration:
                self.chart = Chart.MultiLinesChartContinuousUpdate()
                self.chart.initAxes('epoche', 'reward', 'learning')
                self.isFirstIteration = False
            if self.newLine:
                self.chart.newLine(self.currentLineLabel)
                self.newLine = False
            self.chart.draw(currentReward)


class EarlyStoppRLAgent(keras.callbacks.Callback):

    maxReward = -9999
    avgReward = 0
    epochCounter = 0
    noImprovementCounter = 0
    previousReward = -9999
    patience = 4

    def reset(self):
        self.maxReward = -9999
        self.avgReward = 0
        self.epochCounter = 0
        self.noImprovementCounter = 0
        self.previousReward = -9999

    def on_train_batch_end(self, batch, logs=None):
        print(" For batch {}, reward is {:7.2f} ".format(batch, logs["episode_reward"]))

    def on_test_batch_end(self, batch, logs=None):
        print(" For batch {}, reward is {:7.2f} ".format(batch, logs["episode_reward"]))

    def on_epoch_end(self, epoch, logs=None):
        currentReward = logs["episode_reward"]
        if currentReward > self.maxReward:
            self.maxReward = currentReward
        if currentReward <= self.previousReward:
            self.noImprovementCounter = self.noImprovementCounter + 1
        else:
            self.noImprovementCounter = 0
        if self.noImprovementCounter > self.patience:
            self.model.stop_training = True
        self.epochCounter = self.epochCounter + 1
        self.avgReward = (self.avgReward * (self.epochCounter - 1) + currentReward) / self.epochCounter        
        self.previousReward = currentReward
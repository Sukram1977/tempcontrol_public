# flake8: noqa E302

# openAI gym API
import gym
from gym import spaces

# Keras-RL API
from rl.core import Env

import numpy as np
from queue import Queue
import Regelstrecke
import Chart

class TempControlEnv(Env):

    # Configure model
    modelCfg = Regelstrecke.ModelConfig(100, 200, 300, 50, 20)
    model = Regelstrecke.Model(modelCfg)

    # Environment Parameters
    randomStart = True

    # Settling criteria
    SettlingBand = 3.0   # temperature range +/- about goal temperature
    SettlingSamples = 200 # number of samples needed within band
    tempSettlingQueue = np.zeros(SettlingSamples)

    # Observation Space:
    currentSlope = 0.0
    TempMin = 0.0   
    TempMax = 100.0
    VoltageMin = 0.0
    VoltageMax = model.maxVoltage
    SlopeMin = -2.0
    SlopeMax = 2.0
    # Temperature Goals (for learning algorithms)
    GoalTemperature = 50.0
    # max iterations (Gym environment specification)
    stepCount = 0
    # MaxIterations = 1000

    windowLenght = 600 # samples
    chart = Chart.Moving3SignalsChart(windowLenght, TempMin, TempMax, SlopeMin, SlopeMax, VoltageMin, VoltageMax + 5)
    chart.initHelperLines(GoalTemperature, GoalTemperature-SettlingBand, GoalTemperature+SettlingBand)
    chart.initAxes('Step', 'Temperature', 'Temperature', 'Step', 'Slope', 'Slope', 'Step', 'Voltage', 'Action')

    continuousAS = True
    formerTemperature = 0
    initState = 'ZERO_STATE'
    initStateSeriesLen = 9
    randomInit = False
    doSettlingCheck = True
    resetCounter = 0
    initStateCounter = 0

    def __init__(self, continuousActionsSpace=False):
        if (continuousActionsSpace == True):
            self.continuousAS = True
        super(TempControlEnv, self).__init__()

        #self.action_space = spaces.Discrete(100)  # 0.... 99 volts
        if self.continuousAS:
            self.action_space = spaces.Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)

        # T (temperature), dT
        self.observation_space = spaces.Box(low=np.array([self.TempMin, self.SlopeMin]), high=np.array([self.TempMax, self.SlopeMax]), dtype=np.float32)
        self.model.reset()

    def getReward(self, temperature):
        # calc deviation to goal temperature relatively to the total temperature range:        
        diffTemp = abs(self.GoalTemperature - temperature) / (self.TempMax - self.TempMin)        
        reward = -diffTemp
        return reward


    def step(self, action):
        # Execute one time step within the environment

        # Abbildung actor output layer (linear -1... 1) -> voltage
        voltage = self.VoltageMin + (float(action) + 1)/2 * (self.VoltageMax - self.VoltageMin)
        voltage = min(voltage, self.VoltageMax)
        voltage = max(voltage, self.VoltageMin)

        # print("Step:" + str(action) + str(voltage))

        self.model.step(voltage)

        # Determine observation state
        currentTemperature = self.model.getTemperature()
        if not isinstance(currentTemperature, float):
            print("gotcha")            

        self.currentSlope = currentTemperature - self.formerTemperature
        self.formerTemperature = currentTemperature

        done = False
        # Give some reward before reaching the goal:
        reward = self.getReward(currentTemperature)

        # if currentTemperature > self.TempMax or currentTemperature < self.TempMin:
        #     # we are outside OS specifications
        #     done = True
        # if currentSlope > self.SlopeMax or currentSlope < self.SlopeMin:
        #     # we are outside OS specifications
        #     done = True

        # Definition of goal reached
        goalReached = False
        if self.doSettlingCheck:
            if self.checkSettling(currentTemperature):
                reward = 1 # give max reward
                done = True
                goalReached = True

        self.stepCount = self.stepCount + 1
        # if self.stepCount >= self.MaxIterations:
        #     done = True

        # Truncate state
        currentTemperatureMinimized = min(currentTemperature, self.TempMax)
        currentTemperatureMaximized = max(currentTemperatureMinimized, self.TempMin)        
        self.currentSlope = min(self.currentSlope, self.SlopeMax)
        self.currentSlope = max(self.currentSlope, self.SlopeMin)        
        currentState = np.array([currentTemperatureMaximized, self.currentSlope])

        if str(currentState.shape) == "(2, 1)":
            print("gotcha")

        """ From Keras-RL Env (parente class):
        Returns:
        # observation (object): Agent's observation of the current environment.
        # reward (float) : Amount of reward returned after previous action.
        # done (boolean): Whether the episode has ended, in which case further step() calls will return undefined results.
        # info (dict): Contains auxiliary diagnostic information (helpful for debugging, and sometimes learning).
        """
        info = {"goalReached": goalReached}
        if done:
            print("Done reached")
        return currentState, reward, done, info

    def reset(self):
        # Reset the state of the environment to an initial state
        self.formerTemperature = 0
        self.tempSettlingQueue = np.zeros(self.SettlingSamples)
        self.stepCount = 0
        self.model.reset()
        self.gotoInitialState()
        currentState = np.array([self.model.getTemperature(), self.currentSlope])
        return currentState

    def gotoInitialState(self):
        if self.initState == 'ZERO_STATE':
            return
        if self.initState == 'RANDOM_STATE':
            # every second call of reset triggers a random init values
            self.resetCounter = self.resetCounter + 1            
            if self.resetCounter % 2 == 0:
                numberOfSteps = 201
                i = 0
                actionChangeInterval = 100
                action = np.random.rand() * self.model.maxVoltage
                while i < numberOfSteps:
                    # generate random action between -1 and 1
                    # This space need to correspond to the action space input of step()
                    if i % actionChangeInterval == 0:
                        action = np.random.rand() * 2 - 1
                    self.step(action)
                    i = i + 1
            return
        if self.initState == 'STATE_SERIES':
            heatUpSteps = [0, 50, 100, 0, 50, 100, 0, 50, 100]
            coolDownSteps = [0, 0, 0, 50, 50, 50, 50, 100, 100]
            assert len(heatUpSteps) == self.initStateSeriesLen
            assert len(coolDownSteps) == self.initStateSeriesLen
            if self.initStateCounter > self.initStateSeriesLen:
                self.initStateCounter = 0           
            # heat up some steps
            numberOfRepetitions = heatUpSteps[self.initStateCounter % len(heatUpSteps)]
            for i in range(numberOfRepetitions):
                self.step(1)
            # cool down  some steps
            numberOfRepetitions = coolDownSteps[self.initStateCounter % len(coolDownSteps)]
            for i in range(numberOfRepetitions):
                self.step(0)
            self.initStateCounter = self.initStateCounter + 1

    def render(self, mode='human', close=False):
        # Render the environment to the screen
        self.chart.draw(self.model.getTemperature(), self.currentSlope, self.model.getVoltage(), True)

    def initReward(self, temperature):
        maxRewardWithoutReachingGoal = -0.5
        tempDiff = abs(temperature - self.getGoalTemperature())
        tempDiffNormalized = tempDiff / (self.TempMax - self.TempMin)
        reward = maxRewardWithoutReachingGoal - tempDiffNormalized
        return reward

    def checkSettling(self, temperature):
        self.tempSettlingQueue = np.append(self.tempSettlingQueue, [temperature])
        self.tempSettlingQueue = self.tempSettlingQueue[1:]
        for temp in self.tempSettlingQueue:
            if abs(temp - self.GoalTemperature) > self.SettlingBand:
                return False
        return True

    # Access Space Specifications
    def getActionSpaceDim(self):
        return 1  # voltage

    def getActionSpaceLen(self):
        return 10  # discrete voltage steps        

    def getObervationSpaceDim(self):
        return 2  # T, dT        

    def getObervationSpaceMin(self):
        return [self.TempMin, self.SlopeMin]

    def getObervationSpaceMax(self):
        return [self.TempMax, self.SlopeMax]

    def getGoalTemperature(self):
        return self.GoalTemperature

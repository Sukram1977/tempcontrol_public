import yaml

class ModelConfig(object):
    def __init__(self, cp1, r1, cp2, r2, vmax):
        self.cp1 = cp1
        self.r1 = r1
        self.cp2 = cp2
        self.r2 = r2
        self.vmax = vmax

    def yaml(self):
        return yaml.dump(self.__dict__)

    @staticmethod
    def load(data):
        values = yaml.safe_load(data)
        return ModelConfig(values["cp1"], values["r1"], values["cp2"], values["r2"])

class Model(object):

    # Model:
    # Simple model consisting of 2 capacities
    # like heater capacity connected to a main capacity
    # THe thermal energy path is as follows:
    # - heater converts voltage to heat (energy1)
    # - heater capacity (capacity1) is connected via R1 with the big main capacity
    # - the main capacity looses heat (energy2) to the environment (R2)
    # voltage -> capacity1 -> R1 -> capacity2 -> R2


    # Initial Values:
    energy1 = 0
    energy2 = 0       # Energy of main capacity (capacity)
    voltage = 0

    # Model Configurations:
    capacity1 = 100 # THe main capacity
    R1 = 200    # Resistance between heat capacity1 and capacity2
    capacity2 = 300 # THe main capacity
    R2 = 50    # Resistance between heat capacity and environment
    timeStepSizeSec = 1.0
    stepNr = 0
    minVoltage = 0.0
    maxVoltage = 20.0

    def __init__(self, model):
        if isinstance(model, ModelConfig):
            self.capacity1 = float(model.cp1)
            self.R1        = float(model.r1)
            self.capacity2 = float(model.cp2)
            self.R2        = float(model.r2)
            self.maxVoltage = float(model.vmax)
        self.reset()

    def reset(self):
        self.energy1 = 0       # Energy of heater capacity
        self.energy2 = 0       # Energy of main capacity
        self.voltage = 0
        self.timeStepSizeSec = 1.0
        self.stepNr = 0

    def getStep(self):
        return self.stepNr

    def getTemperature(self):
        currentTemperature = self.energy2 / self.capacity2
        if not isinstance(currentTemperature, float):
            print("gotcha")
        return currentTemperature

    def getVoltage(self):
        return self.voltage

    # update model
    # supposed to be called in a time constant period
    def step(self, voltageIn): # noqa: E301
        self.stepNr = self.stepNr + 1
        if not isinstance(voltageIn, float):
            voltage = voltageIn[0]
        else:
            voltage = voltageIn
        self.voltage = max(voltage, self.minVoltage)
        self.voltage = min(voltage, self.maxVoltage)
        temperature1 = self.energy1 / self.capacity1
        temperature2 = self.energy2 / self.capacity2
        dtemp = temperature1 - temperature2
        heatFlow1 = (dtemp * dtemp)  / self.R1
        heatFlow2 = temperature2 * temperature2 / self.R2
        self.energy1 = max(self.energy1 + self.voltage * self.voltage - heatFlow1 * self.timeStepSizeSec, 0)
        self.energy2 = max(self.energy2 + (heatFlow1 - heatFlow2) * self.timeStepSizeSec, 0)

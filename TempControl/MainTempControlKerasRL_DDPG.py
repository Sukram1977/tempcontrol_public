# Temp control learner
# Using Keras RL2, DDPG Agent
# Example: https://github.com/keras-rl/keras-rl/blob/master/examples/ddpg_pendulum.py

import os
import numpy as np
from pathlib import Path

import TempControlGymEnv
import KerasCustomCallbacks
import AgentAnalyser

from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Activation, Flatten, Input, Concatenate
from tensorflow.keras.optimizers import Adam
# from keras.callbacks import TensorBoard  -> not working for RL?
# from keras.callbacks import EarlyStopping

# from rl.agents import DDPGAgent
import ddpg
from rl.memory import SequentialMemory
from rl.random import OrnsteinUhlenbeckProcess
from datetime import datetime

RUN_AND_SHOW_DONT_LEARN = False
ANALYSE_AGENT = False

ENV_NAME = 'TempControl-v0'

# Environment
env = TempControlGymEnv.TempControlEnv(True)

# Action Space: space of possible actions
print(repr(len(env.action_space.shape)))
assert len(env.action_space.shape) == 1
nb_actions = env.action_space.shape[0]

# Define call backs
# log_dir = "./Log/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
# tensorboard = TensorBoard(log_dir=log_dir, histogram_freq=1)
customPrinting = KerasCustomCallbacks.LossAndErrorPrintingCallback()
# customEarlyStopping = KerasCustomCallbacks.EarlyStoppRLAgent()
# early_stop = EarlyStopping(monitor='episode_reward', patience=5) # epochs stagnation before termination   

# Output path setting
now = datetime.now()
# timestampRaw = datetime.timestamp(now)
timestamp = now.strftime('%Y%m%d_%H%M%S')
subDirName = "./Log/{}".format(timestamp)
if not RUN_AND_SHOW_DONT_LEARN:
    os.mkdir(subDirName)

# Config setting
configId = 0
if RUN_AND_SHOW_DONT_LEARN:
    fixConfig = 1
    configId = fixConfig
warmUpList = [100, 200, 500]
actorLayersList = [2, 2, 2, 3, 3, 3]
isFirstIteration = True

try: # use try to have a contolled exit by pressing ctrl-c
    while True:
        configId = configId + 1

        print("Iteration Config {} ####################".format(configId))

        warmUp = warmUpList[configId % len(warmUpList)]
        actorLayers = actorLayersList[configId % len(actorLayersList)]

        # Actor
        actor = Sequential()
        flatten_input = Flatten(input_shape=(1,) + env.observation_space.shape)
        actor.add(flatten_input)
        actor.add(Dense(16))
        actor.add(Activation('relu'))
        if (actorLayers > 1):
            actor.add(Dense(16))
            actor.add(Activation('relu'))
        if (actorLayers > 2):
            actor.add(Dense(16))
            actor.add(Activation('relu'))
        actor.add(Dense(nb_actions))
        actor.add(Activation('linear'))
        print(actor.summary())

        # Critic
        action_input = Input(shape=(nb_actions,), name='action_input')
        observation_input = Input(shape=(1,) + env.observation_space.shape, name='observation_input')
        flattened_observation = Flatten()(observation_input)
        x = Concatenate()([action_input, flattened_observation])
        x = Dense(32)(x)
        x = Activation('relu')(x)
        x = Dense(32)(x)
        x = Activation('relu')(x)
        x = Dense(32)(x)
        x = Activation('relu')(x)
        x = Dense(1)(x)
        x = Activation('linear')(x)
        critic = Model(inputs=[action_input, observation_input], outputs=x)
        print(critic.summary())

        # Finally, we configure and compile our agent. You can use every built-in Keras optimizer and
        # even the metrics!

        memory = SequentialMemory(limit=100000, window_length=1)
        random_process = OrnsteinUhlenbeckProcess(size=nb_actions, theta=.15, mu=0., sigma=.3)
        agent = ddpg.DDPGAgent(nb_actions=nb_actions, actor=actor, critic=critic, critic_action_input=action_input,
                        memory=memory, nb_steps_warmup_critic=warmUp, nb_steps_warmup_actor=warmUp,
                        random_process=random_process, gamma=.99, target_model_update=1e-3)
        agent.compile(Adam(lr=.001, clipnorm=1.), metrics=['mae'])


        if RUN_AND_SHOW_DONT_LEARN:
            # agent.critic.load_weights('./Log/20200927_131744/Config1_ddpg_Reward -25.04_weights_critic.h5f')
            # agent.actor.load_weights('./Log/20200927_131744/Config1_ddpg_Reward -25.04_weights_actor.h5f')
            fileName = 'Config1_ddpg_Reward -58.16_Iterations21_weights_actor.h5f'
            folderName = '20201002_213635'
            agent.critic.load_weights('./Log/{}/{}critic.h5f'.format(folderName, fileName[:-9]))
            agent.actor.load_weights('./Log/{}/{}actor.h5f'.format(folderName, fileName[:-9]))
            # agent.critic.load_weights('./Log/20200910_084703/ddpg_Reward -38.30_Config1_weights_critic.h5f')
            # agent.actor.load_weights('./Log/20200910_084703/ddpg_Reward -38.30_Config1_weights_actor.h5f')
            # Config3_ddpg_Reward -38.66_weights_actor.h5f
            env.initState = 'ZERO_STATE'
            env.doSettlingCheck = False
            agent.test(env, nb_episodes=1, visualize=True, nb_max_episode_steps=20000)
            analyser = AgentAnalyser.DDPGAnalyser(agent.actor, agent.critic)
            analyser.run()
            exit()

        # In order to control the training and save weights if the agent improved
        # we split the training (fit call) in several rounds
        numberOfStepsPerEpisode = 600
        worst_reward = -numberOfStepsPerEpisode
        best_reward = worst_reward - 1
        last_reward = worst_reward - 1
        continue_training = True
        training_interval = 0
        patience = 3
        patience_counter = 0
        weightsSaved = False
        customPrinting.doPlot(True)
        customPrinting.plotNewLine("Config{}".format(configId))
        bestRewardFileName = ''
        while continue_training:
            # Okay, now it's time to learn something! We visualize the training here for show, but this
            # slows down training quite a lot. You can always safely abort the training prematurely using
            # Ctrl + C.
            customPrinting.reset()
            env.initState = 'STATE_SERIES'
            env.doSettlingCheck = True
            numberOfEpisodesPerFit = env.initStateSeriesLen
            agent.fit(env, nb_steps=numberOfEpisodesPerFit * numberOfStepsPerEpisode, visualize=False, verbose=0, nb_max_episode_steps=numberOfStepsPerEpisode, callbacks=[customPrinting])
            if customPrinting.avgReward > best_reward:
                best_reward = customPrinting.avgReward
                bestRewardFileName = '{}/Config{}_ddpg_Reward{:7.2f}_Iterations{}_weights.h5f'.format(subDirName, configId, best_reward, training_interval)
                agent.save_weights(bestRewardFileName, overwrite=True)
                weightsSaved = True
            agent_has_improved = customPrinting.avgReward > last_reward
            if not agent_has_improved:
                patience_counter = patience_counter + 1
            else:
                patience_counter = 0
            continue_training = patience_counter < patience
            training_interval = training_interval + 1
            last_reward = customPrinting.avgReward
            Path(subDirName).mkdir(parents=True, exist_ok=True)
            customPrinting.savePlot('{}/EpocheChart.png'.format(subDirName))
            print("Iteration=config id:{},  Interval: {}, current avg: {} best avg: {} patience counter: {}".format(configId, training_interval, customPrinting.avgReward, best_reward, patience_counter))

        if isFirstIteration:
            f = open("{}/{}_TempControlDDPG_logFile.txt".format(subDirName, timestamp), "w+")
            f.write("Start\n")
        f.write("Iteration=config id: {}, warmUp: {}, actorLayers: {}, bestReward:{}\n".format(configId, warmUp, actorLayers, best_reward))
        f.flush()

        # Finally, evaluate our algorithm using best set of weights found in this iteration:
        if weightsSaved:
            fileEnding = bestRewardFileName[-4:] # this is typically == '.h5f'
            fileNameActor = '{}_actor{}'.format(bestRewardFileName[:-4], fileEnding)
            fileNameCritic = '{}_critic{}'.format(bestRewardFileName[:-4], fileEnding)
            agent.critic.load_weights(fileNameCritic)
            agent.actor.load_weights(fileNameActor)
            agent.test(env, nb_episodes=1, visualize=True, nb_max_episode_steps=numberOfStepsPerEpisode)

        isFirstIteration = False
except KeyboardInterrupt:
    pass

# env.close()
f.close()
import numpy as np


class ReinforceLearner:

    LEARNING_RATE = 0.2  # between 0...1
    DISCOUNT = 0.9  # between 0...1,  a value how much we value future reward

    AQ_PAIR_A_INDEX = 0  # Index for action
    AQ_PAIR_Q_INDEX = 1  # Index for Q


    state_space_size = 0
    action_space_size = 1

    current_state = 0
    action = 1

    # Action space size = 1 means continuous space
    def __init__(self, state_space_size, action_space_size):
        self.state_space_size = state_space_size
        self.action_space_size = action_space_size
        self.reset("")

    def reset(self, qTableFileStr):

        # Init Q-Table
        # Initial values: Low=-2 und high=0: Weil Rewards sind zwischen -1 (kein reward) und 0 (=Erfolg)
        #     je grösser die Werte, je eher wird exploration unterstützt, weil die begangenen actions kleiner werden (see Wiki, Q-learning)
        # Size: 20 x 20 x 3 (Observation space x action space). Die Q-Tabelle soll für jede Kombination aus Zustand und Action einen Q-Wert speichern 
        self.q_table = np.random.uniform(low=-1, high=1, size=(self.state_space_size + [self.action_space_size]))
        print('Init Q-Table with ' + repr(self.q_table.shape) + ' elements (' + repr(self.q_table.size * self.q_table.itemsize) + ' Bytes)')

        # ...or load table if file-name is valid
        if (qTableFileStr):
            self.q_table = np.load(qTableFileStr)
            print("Table loaded from: " + qTableFileStr)


    def getAction(self, current_state, epsilon):
        if np.random.random() > epsilon: 
            # Get action from Q table
            self.action = np.argmax(self.q_table[current_state])    
        else:
            # Get random action
            self.action = np.random.randint(0, self.action_space_size)        
        return self.action

    # get action in continuous action space
    # def getActionCAS(self, current_state, epsilon):
    #     if np.random.random() > epsilon: 
    #         # Get action from Q table
    #         aq_pair_array = self.q_table[current_state] # the a/q-values of the current state
    #         # Extrapolate next action
    #         action1 = aq_pair_array[0, 0]
    #         action2 = aq_pair_array[1, 0]

    #         self.action = np.argmax(self.q_table[current_state])    
    #     else:
    #         # Get random action
    #         self.action = np.random.randint(0, self.action_space_size)        
    #     return self.action


    # reward: reward received by reeaching new state
    # done: if simulation has ended
    # goalReached: weather goal has been reached
    def update(self, reward, done, current_state, new_state, goalReached): # noqa: E301

        # Bemerkung: In python wird mit (x,) ein neuer Tuple mit einem Element erstellt. So können tuples gemerged werden
        q_coordinate_of_current_state_action = tuple(current_state + (self.action, ))

        # update former q value
        if not done:
            new_q_values = self.q_table[new_state] # the q-values of the next state (action space dimensions)
            max_new_q = np.max(new_q_values)

            current_q = self.q_table[q_coordinate_of_current_state_action]

            # calculate new q value for the action based on current q, new q and reward
            new_q = (1 - self.LEARNING_RATE) * current_q + self.LEARNING_RATE * (reward + self.DISCOUNT * max_new_q)

            # update q
            self.q_table[q_coordinate_of_current_state_action] = new_q
        elif goalReached: # we reached the goal
            self.q_table[q_coordinate_of_current_state_action] = 0 # set q to maximum
            reward = 0

    # update in case of continuous action space
    # def updateCAS(self, reward, done, current_state, new_state, goalReached):

    #     coordinate_of_current_state_action = tuple(current_state + (self.action, ))

    #     # update former q value
    #     if not done:
    #         new_aq_pair_array = self.q_table[new_state] # the a/q-values of the next state
    #         next_aq_pair = new_aq_pair_array.max(axis=self.AQ_PAIR_Q_INDEX)
    #         next_action_q = next_aq_pair[self.AQ_PAIR_Q_INDEX]

    #         current_aq_pair = self.q_table[coordinate_of_current_state_action]

    #         # calculate new q value for the action based on current q, new q and reward
    #         new_q = (1 - self.LEARNING_RATE) * current_q + self.LEARNING_RATE * (reward + self.DISCOUNT * max_new_q)

    #         # update q
    #         self.q_table[coordinate_of_current_state_action] = new_q
    #     elif goalReached: # we reached the goal
    #         self.q_table[coordinate_of_current_state_action] = 0 # set q to maximum
    #         reward = 0



    def serializeTable(self, fileStr):        
        np.save(fileStr, self.q_table)
        print("Table stored to: " + fileStr)

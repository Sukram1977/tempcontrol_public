import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np


class simpleXYChart:
    lineLabels = []

    def __init__(self, xlabel, ylabel, title, legendTitle=""):
        self.fig = plt.figure()
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.title = title
        self.legendTitle = legendTitle

    def draw(self, x_array, y_array, legendTxt):
        plt.figure(self.fig.number)
        plt.clf()
        plt.xlabel(self.xlabel)
        plt.ylabel(self.ylabel)
        plt.title(self.title)
        self.lineLabels.append(str(legendTxt))
        if len(x_array) == len(y_array):
            plt.plot(x_array, y_array)

    def addLine(self, x_array, y_array, legendTxt):
        plt.figure(self.fig.number)
        self.lineLabels.append(str(legendTxt))
        if len(x_array) == len(y_array):
            plt.plot(x_array, y_array)
            plt.legend(self.lineLabels, title=self.legendTitle)

    def show(self):
        plt.figure(self.fig.number)
        mngr = plt.get_current_fig_manager()
        mngr.window.setGeometry(100, 700, 640, 545)
        plt.show()
        # plt.draw()
        # plt.pause(1e-17)


class MultiLinesChartContinuousUpdate:
    isValid = False
    yBuffers = [] # the list of line buffers (type: list of list)
    lineLabels = []
    y1_buff = []
    # plt.ion()

    def __init__(self):
        self.fig = plt.figure()
        mngr = plt.get_current_fig_manager()
        # to put it into the upper left corner for example:
        mngr.window.setGeometry(50, 100, 640, 545)

    def initAxes(self, xlabel, ylabel, title):
        # Add helper lines:
        plt.figure(self.fig.number)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(title)

    def newLine(self, label):
        self.isValid = True # newLine needs to be called before calling draw
        self.lineLabels.append(label)
        self.yBuffers.append([]) # append empty list

    def draw(self, y):
        if self.isValid:
            plt.figure(self.fig.number)
            plt.clf()
            # draw to last buffer (get last list entry with -1 = current line):
            self.yBuffers[-1].append(y)
            for line in self.yBuffers:
                x_array = np.arange(1, len(line) + 1)
                plt.plot(x_array, np.array(line))
            plt.legend(self.lineLabels)
            plt.draw()
            plt.pause(1e-17)

    def save(self, fileName):
        plt.figure(self.fig.number)
        plt.savefig(fileName)

class MovingSignalChart:

    bufferSize = 300
    plotBuffer = np.zeros(bufferSize)
    # plt.ion()  # interactive mode, redraw after each call of draw
    #ax = plt.gca()
    h1 = {}  # handle to current plot

    def __init__(self, bufferSize):
        self.fig = plt.figure()
        self.bufferSize = bufferSize
        plt.figure(self.fig.number)
        self.h1, = self.ax.plot(np.arange(0, bufferSize), self.plotBuffer)

    def initHelperLines(self, goalTemp, settlingMin, settlingMax):
        # Add helper lines:
        plt.figure(self.fig.number)
        self.ax.plot([0, self.bufferSize], [goalTemp, goalTemp], '--')
        self.ax.plot([0, self.bufferSize], [settlingMin, settlingMin], '--')
        self.ax.plot([0, self.bufferSize], [settlingMax, settlingMax], '--')

    def draw(self, newvalue, doPauseForRedraw):
        # shift all values in array one step right:
        plt.figure(self.fig.number)
        self.plotBuffer[1:] = self.plotBuffer[:-1]
        self.plotBuffer[0] = newvalue
        self.h1.set_xdata(np.arange(0, self.bufferSize))
        self.h1.set_ydata(self.plotBuffer)
        self.ax.set_ylim([0, 100])  # limits the data
        # ax.relim() # limits the data
        # if (max(plotBuffer) - min(plotBuffer)) > 1:
        #    ax.autoscale_view() # limits the view

        plt.draw()
        if doPauseForRedraw:
            plt.pause(1e-17)

class Moving2SignalsChart:

    #plt.ion()  # interactive mode, redraw after each call of draw

    def __init__(self, bufferSize, minY1, maxY1, minY2, maxY2):
        self.fig, self.axs = plt.subplots(nrows=2, ncols=1)
        mngr = plt.get_current_fig_manager()
        mngr.window.setGeometry(1200, 100, 640, 545)
        self.bufferSize1 = bufferSize
        self.bufferSize2 = bufferSize
        self.plotBuffer1 = np.zeros(self.bufferSize1)
        self.plotBuffer2 = np.zeros(self.bufferSize2)
        self.minY1 = minY1
        self.maxY1 = maxY1
        self.minY2 = minY2
        self.maxY2 = maxY2
        self.h1, = self.axs[0].plot(np.arange(0, bufferSize), self.plotBuffer1)
        self.h2, = self.axs[1].plot(np.arange(0, bufferSize), self.plotBuffer2)

    def initHelperLines(self, goalTemp, settlingMin, settlingMax):
        # Add helper lines:
        plt.figure(self.fig.number)
        self.axs[0].plot([0, self.bufferSize1], [goalTemp, goalTemp], '--')
        self.axs[0].plot([0, self.bufferSize1], [settlingMin, settlingMin], '--')
        self.axs[0].plot([0, self.bufferSize1], [settlingMax, settlingMax], '--')

    def initAxes(self, ax1_xlabel, ax1_ylabel, ax1_title, ax2_xlabel, ax2_ylabel, ax2_title):
        # Add helper lines:
        plt.figure(self.fig.number)
        self.axs[0].set_xlabel(ax1_xlabel)
        self.axs[0].set_xlabel(ax1_xlabel)
        self.axs[0].set_title(ax1_title)
        self.axs[1].set_xlabel(ax2_xlabel)
        self.axs[1].set_xlabel(ax2_xlabel)
        self.axs[1].set_title(ax2_title)

    def draw(self, newvalue1, newvalue2, doPauseForRedraw):
        # shift all values in array one step right:
        plt.figure(self.fig.number)
        self.plotBuffer1[1:] = self.plotBuffer1[:-1]
        self.plotBuffer1[0] = newvalue1
        self.plotBuffer2[1:] = self.plotBuffer2[:-1]
        self.plotBuffer2[0] = newvalue2
        self.h1.set_xdata(np.arange(0, self.bufferSize1))
        self.h1.set_ydata(self.plotBuffer1)
        self.h2.set_xdata(np.arange(0, self.bufferSize2))
        self.h2.set_ydata(self.plotBuffer2)
        self.axs[0].set_ylim([self.minY1, self.maxY1])  # limits the data
        self.axs[1].set_ylim([self.minY2, self.maxY2])  # limits the data

        plt.draw()
        if doPauseForRedraw:
            plt.pause(1e-17)

class Moving3SignalsChart:

    #plt.ion()  # interactive mode, redraw after each call of draw

    def __init__(self, bufferSize, minY1, maxY1, minY2, maxY2, minY3, maxY3):
        self.fig, self.axs = plt.subplots(nrows=3, ncols=1)
        mngr = plt.get_current_fig_manager()
        mngr.window.setGeometry(1200, 100, 640, 545)        
        self.bufferSize1 = bufferSize
        self.bufferSize2 = bufferSize
        self.bufferSize3 = bufferSize
        self.plotBuffer1 = np.zeros(self.bufferSize1)
        self.plotBuffer2 = np.zeros(self.bufferSize2)
        self.plotBuffer3 = np.zeros(self.bufferSize3)
        self.minY1 = minY1
        self.maxY1 = maxY1
        self.minY2 = minY2
        self.maxY2 = maxY2
        self.minY3 = minY3
        self.maxY3 = maxY3
        self.h1, = self.axs[0].plot(np.arange(0, bufferSize), self.plotBuffer1)
        self.h2, = self.axs[1].plot(np.arange(0, bufferSize), self.plotBuffer2)
        self.h3, = self.axs[2].plot(np.arange(0, bufferSize), self.plotBuffer3)

    def initHelperLines(self, goalTemp, settlingMin, settlingMax):
        # Add helper lines:
        plt.figure(self.fig.number)
        self.axs[0].plot([0, self.bufferSize1], [goalTemp, goalTemp], '--')
        self.axs[0].plot([0, self.bufferSize1], [settlingMin, settlingMin], '--')
        self.axs[0].plot([0, self.bufferSize1], [settlingMax, settlingMax], '--')

    def initAxes(self, ax1_xlabel, ax1_ylabel, ax1_title, ax2_xlabel, ax2_ylabel, ax2_title, ax3_xlabel, ax3_ylabel, ax3_title):
        # Add helper lines:
        plt.figure(self.fig.number)
        self.axs[0].set_xlabel(ax1_xlabel)
        self.axs[0].set_xlabel(ax1_xlabel)
        self.axs[0].set_title(ax1_title)
        self.axs[1].set_xlabel(ax2_xlabel)
        self.axs[1].set_xlabel(ax2_xlabel)
        self.axs[1].set_title(ax2_title)
        self.axs[2].set_xlabel(ax3_xlabel)
        self.axs[2].set_xlabel(ax3_xlabel)
        self.axs[2].set_title(ax3_title)

    def draw(self, newvalue1, newvalue2, newvalue3, doPauseForRedraw):
        # shift all values in array one step right:
        plt.figure(self.fig.number)
        self.plotBuffer1[1:] = self.plotBuffer1[:-1]
        self.plotBuffer1[0] = newvalue1
        self.plotBuffer2[1:] = self.plotBuffer2[:-1]
        self.plotBuffer2[0] = newvalue2
        self.plotBuffer3[1:] = self.plotBuffer3[:-1]
        self.plotBuffer3[0] = newvalue3
        self.h1.set_xdata(np.arange(0, self.bufferSize1))
        self.h1.set_ydata(self.plotBuffer1)
        self.h2.set_xdata(np.arange(0, self.bufferSize2))
        self.h2.set_ydata(self.plotBuffer2)
        self.h3.set_xdata(np.arange(0, self.bufferSize3))
        self.h3.set_ydata(self.plotBuffer3)
        self.axs[0].set_ylim([self.minY1, self.maxY1])  # limits the data
        self.axs[1].set_ylim([self.minY2, self.maxY2])  # limits the data
        self.axs[2].set_ylim([self.minY3, self.maxY3])  # limits the data

        plt.draw()
        if doPauseForRedraw:
            plt.pause(1e-17)            
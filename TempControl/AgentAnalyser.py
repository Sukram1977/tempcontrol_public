import numpy as np
from tensorflow.keras.models import Sequential
import Chart

class DDPGAnalyser:

    critic = Sequential()
    actor  = Sequential()

    def __init__(self, actor, critic):
        self.actor = actor
        self.critic = critic
        self.chart = Chart.simpleXYChart('state', 'action', 'actor', 'slope')

    def process_state_batch(self, batch):
        batch = np.array(batch)
        return batch

    def select_action(self, state):
        stateList = [state]
        stateArray = np.array(stateList)
        batch = self.process_state_batch([stateArray])
        action_temp = self.actor.predict_on_batch(batch)
        action = action_temp.flatten()
        return action

    def run(self):
        stateList = [[1, 1]] # list of states with onyl one state at position stateList[0]
        slopeList = np.linspace(-1, 1, 11)
        slopeListRounded = [round(x, 1) for x in slopeList]
        isFirstLine = True
        for slope in slopeListRounded:
            stateList = self.generateStateList(slope)
            actionList = []
            for state in stateList:
                action = self.select_action(state)
                action = min(action, 1)   # crop action to -1 and 1
                action = max(action, -1)
                actionList.append(action)
            stateLists = list(zip(*stateList)) # "unzip" list of tuples in lists for every dimension
            if (isFirstLine):
                self.chart.draw(stateLists[0], actionList, slope)
                isFirstLine = False
            else:
                self.chart.addLine(stateLists[0], actionList, slope)
        self.chart.show()

    def generateStateList(self, slope):
        # todo: make the following parameters configurable - this is an example set of TempControl environment
        minTemp = 0
        maxTemp = 60
        tempRange = range(minTemp, maxTemp)
        slopeList = [slope] * len(tempRange)
        stateList = list(zip(tempRange, slopeList)) # zip function concenates two single value lists into a list of tuples
        return stateList
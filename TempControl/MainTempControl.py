
import Regelstrecke
import yaml
import TempControlGui
import Chart
import numpy as np

# pylint: disable=E1136  # pylint/issues/3139

# Initializations
waitTimeMs = 10 # ms
windowLenght = 500 # samples

# configData = {
#     'simulation':{'Tsoll': 50},
#     'model':{'cp1':10,'r1':200,'cp2':50,'r2':20}}

modelCfg = Regelstrecke.ModelConfig(100, 200, 300, 50, 20)

Tsoll = 50
SettlingBand = 5.0

# CHeck for config file
try:
    with open("modelConfig.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
        modelCfg.load(cfg)
except Exception:
    # no config file found - make a new one
    with open("modelConfig.yml", 'w') as ymlfile:
        cfgData = modelCfg.yaml()
        yaml.dump(cfgData, ymlfile)


model = Regelstrecke.Model(modelCfg)
gui   = TempControlGui.TempControlGui(modelCfg, Tsoll)

# chart = Chart.MovingSignalChart(windowLenght)
TempMin = 0.0
TempMax = 100.0
VoltageMin = model.minVoltage
VoltageMax = model.maxVoltage
chart = Chart.Moving2SignalsChart(windowLenght, TempMin, TempMax, VoltageMin, VoltageMax + 5)
chart.initHelperLines(Tsoll, Tsoll - SettlingBand, Tsoll + SettlingBand)
chart.initAxes('Step', 'Environment State - Temperature', 'Temperature', 'Step', 'Voltage', 'Action')

pause = False
autoEnable = False
errorSum = 0
errorTempOld = 0

# init states
initStateSeriesLen = 9
initStateCounter = 0

# THE main loop
while True:
    event, values = gui.read(waitTimeMs)
    if event in (None, 'Cancel'):	# if user closes window or clicks cancel
        break
    if event == 'ApplyModel':
        modelCfg.cp1 = values['INPUT_CP1']
        modelCfg.cp2 = values['INPUT_CP2']
        modelCfg.r1 = values['INPUT_R1']
        modelCfg.r2 = values['INPUT_R2']
        modelCfg.vmax = values['INPUT_VMAX']        
        model.__init__(modelCfg)
    if event == 'Pause':
        pause = not pause
    if event == 'Reset':
        errorSum = 0
    if event == 'Enable':
        autoEnable = not autoEnable
        if autoEnable:
            gui.set('OUT_AUTOENABLE', 'ON')
        else:
            gui.set('OUT_AUTOENABLE', 'OFF')
    if event == 'Random':
        model.reset()
        heatUpSteps = [0, 50, 100, 0, 50, 100, 0, 50, 100]
        coolDownSteps = [0, 0, 0, 50, 50, 50, 50, 100, 100]
        if initStateCounter > initStateSeriesLen:
            initStateCounter = initStateSeriesLen
        # heat up some steps
        numberOfRepetitions = heatUpSteps[initStateCounter % len(heatUpSteps)]
        for i in range(numberOfRepetitions):
            model.step(model.maxVoltage)
        # cool down  some steps
        numberOfRepetitions = coolDownSteps[initStateCounter % len(coolDownSteps)]        
        for i in range(numberOfRepetitions):
            model.step(0.0)
        initStateCounter = initStateCounter + 1


    voltagePerCent  = float(values['SLIDER1'])
    TempSoll = float(values['IN_SOLL'])
    TempIst  = model.getTemperature()
    errorTemp = TempSoll - TempIst
    dError = errorTemp - errorTempOld
    errorTempOld = errorTemp

    # PID control
    if autoEnable:
        Kp = float(values['IN_P'])
        Kd = float(values['IN_D'])
        Ki = float(values['IN_I'])
        voltagePerCent = Kp * errorTemp + Kd * (dError)
        gui.set('SLIDER1', voltagePerCent)

    if not pause:
        # step model:
        voltage = float(voltagePerCent) / 100.0 * model.maxVoltage
        model.step(voltage)
        # evaluate reward (reward max = 0 (success), min = -1 (penalty)
        MAX_TEMP_ERROR = 50
        reward = -(MAX_TEMP_ERROR / abs(errorTemp))

        errorSum = errorSum + (abs(errorTemp))
        chart.draw(model.getTemperature(), voltage, True)
        gui.set('OUT_FEHLER', errorSum)
        gui.set('OUT_TEMP_SOLL', TempSoll)
        gui.set('OUT_TEMP_IST', TempIst)
        gui.set('OUT_TEMP_ERROR', errorTemp)
        gui.set('OUT_TEMP_DERROR', dError)
        gui.set('OUT_STEP', model.getStep())
    # print('You entered ', values[0], values['SLIDER1'])

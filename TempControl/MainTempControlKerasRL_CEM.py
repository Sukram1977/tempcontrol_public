# Temp control learner
# Based on Q-Learning but with continuous action space

import numpy as np
import TempControlGymEnv

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Flatten
# from tensorflow.keras.optimizers import Adam

from rl.agents.cem import CEMAgent
from rl.memory import EpisodeParameterMemory
from rl.policy import LinearAnnealedPolicy
from rl.policy import EpsGreedyQPolicy

RUN_AND_SHOW_DONT_LEARN = False
SERIALIZE_BEST_TABLE = False

EPISODES = 5000
RENDER_EVERY = EPISODES
RENDER_FIRST_EPISODE = True
STATUS_EVERY = 200
SERIALIZE_TABLE_EVERY = 1000

ENV_NAME = 'TempControl-v0'

# Environment
env = TempControlGymEnv.TempControlEnv()

# Observation Space: states in this environment
OS_MAX = np.array(env.getObervationSpaceMax())
OS_MIN = np.array(env.getObervationSpaceMin())
OBSERVATION_SPACE_DIM = env.getObervationSpaceDim()
OBSERVATION_SPACE_DIM_GYM_API = len(env.observation_space.high)
OBSERVATION_SPACE_LEN = 20
discret_OS_size_array = [OBSERVATION_SPACE_LEN] * OBSERVATION_SPACE_DIM # = list mit (20, 20)
discrete_os_step_width_array = (OS_MAX - OS_MIN) / discret_OS_size_array

# Action Space: space of possible actions
ACTION_SPACE_DIM = env.getActionSpaceDim()
ACTION_SPACE_LEN = env.getActionSpaceLen()
ACTION_SPACE_LEN_GYM_API = env.action_space.n

# Option 1 : Simple model
windowLength = 1  # number of states over time to be combined to from a new state?
model = Sequential()
model.add(Flatten(input_shape=(windowLength,) + env.observation_space.shape))
model.add(Dense(ACTION_SPACE_LEN))
model.add(Activation('softmax'))

print(model.summary())

# Configure and compile our agent:
# Policy: Set Epsilon Greedy poliy EpsGreedyQPolicy() to balance "exploration and exploitation"
# policy = LinearAnnealedPolicy(EpsGreedyQPolicy(), attr='eps', value_max=1., value_min=.1, value_test=.05, nb_steps=10000)
memory = EpisodeParameterMemory(limit=1000, window_length=windowLength)
cem = CEMAgent(model=model, nb_actions=ACTION_SPACE_LEN, memory=memory, 
               batch_size=50, nb_steps_warmup=2000, train_interval=50, elite_frac=0.05)
cem.compile() # 'adam', metrics=['loss function']

# Okay, now it's time to learn something! We visualize the training here for show, but this
# slows down training quite a lot. You can always safely abort the training prematurely using
# Ctrl + C.
cem.fit(env, nb_steps=100000, visualize=False, verbose=2)

# After training is done, we save the best weights.
cem.save_weights('cem_{}_params.h5f'.format(ENV_NAME), overwrite=True)

# Finally, evaluate our algorithm for 5 episodes.
cem.test(env, nb_episodes=5, visualize=True)

env.close()
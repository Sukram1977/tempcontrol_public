# Q-learnin example
# Mostly based on the car / flag gym tutorial:
# According to https://www.youtube.com/watch?v=Gq1Azv_B4-4
# https://pythonprogramming.net/q-learning-algorithm-reinforcement-learning-python-tutorial/
# Q learning: Q stands for quality of a state-action combination

# pip install gym
import gym
import numpy as np

import ReinforceLearner

RUN_AND_SHOW_DONT_LEARN = True
SERIALIZE_BEST_TABLE = False

EPISODES = 5000
RENDER_EVERY = EPISODES
RENDER_FIRST_EPISODE = False
STATUS_EVERY = 200
SERIALIZE_TABLE_EVERY = 1000

env = gym.make("MountainCar-v0")
env.reset()

# QLearning(env, 0.2, 0.9, 0.8, 0, 5000)
# QLearning(env, learning, discount, epsilon, min_eps, episodes):

# q-Table file name
doLoadTable = True
qTableFileStr = './tempData/table.npy'

# Space of states in this environment
# print(env.observation_space.high) # Highest value of all observations (states)
# print(env.observation_space.low)  # Lowest value of all observations (states)
# print(env.action_space.n) # how many actions
OBSERVATION_SPACE_DIM = len(env.observation_space.high) # = 2 (Anzahl Dimensionen "Observation values" des Zustandsraums zB Geschwindigkeit und Ort)
DISCRETE_OS_NUMBER_OF_STEPS = 20
discret_OS_size_array = [DISCRETE_OS_NUMBER_OF_STEPS] * OBSERVATION_SPACE_DIM # = Vector mit (20 / 20)
discrete_os_step_width_array = (env.observation_space.high - env.observation_space.low) / discret_OS_size_array

# Exploration settings
# Epsilon: Wieviel Zufall bei Auswahl der "action" im Spiel sein soll
epsilon = 0.8 # between 0.0 and 1.0. Start value not a constant, going to be decayed
START_EPSILON_DECAYING = 1
END_EPSILON_DECAYING = 1000
epsilon_decay_value = epsilon / (END_EPSILON_DECAYING - START_EPSILON_DECAYING)
END_EPSILON_DECAYING = EPISODES // 2 # "//" = floor division operator

if RUN_AND_SHOW_DONT_LEARN:
    doLoadTable = True
    RENDER_EVERY = EPISODES
    RENDER_FIRST_EPISODE = True
    epsilon = 0.0

rf = ReinforceLearner.ReinforceLearner(discret_OS_size_array, env.action_space.n)

# load table
if not doLoadTable:
    qTableFileStr = ""

rf.reset(qTableFileStr)


def get_discrete_state(state):
    discrete_state = (state - env.observation_space.low) / discrete_os_step_width_array
    return tuple(discrete_state.astype(np.int))


# Initialize variables to track rewards
reward_list = []
bestScoreLastSerialization = -200

for episode in range(EPISODES):

    render = False
    if episode % RENDER_EVERY == 0:
        render = True
    if episode == 0 and not RENDER_FIRST_EPISODE:
        render = False
    if episode > (EPISODES - 2):
        render = True

    current_state = get_discrete_state(env.reset()) # reset returns the initial state
    # print(np.argmax(q_table[current_state]))

    # Start new episode --------------------------------------------------------------------
    done = False
    tot_rewards = 0
    while not done:
        # State model:
        # current_state ---- action ----> new_state
        # current_q                       new_q_values

        action = rf.getAction(current_state, epsilon)

        new_state_raw, reward, done, _ = env.step(action) # done is true after 200 steps or if goal is reached

        if render:
            env.render()

        new_state = get_discrete_state(new_state_raw)

        if not RUN_AND_SHOW_DONT_LEARN:
            rf.update(reward, done, current_state, new_state, new_state_raw[0] >= env.goal_position)

        tot_rewards += reward
        current_state = new_state
    # End of Episode --------------------------------------------------------------------
    reward_list.append(tot_rewards)

    if episode % STATUS_EVERY == 0:
        averageRewardSinceLastStatus = np.mean(reward_list)
        print('Episode Nr: ' + repr(episode) + ' Average reward: ' + repr(averageRewardSinceLastStatus) + ' Epsilon: ' + repr(epsilon))
        reward_list.clear()

    doSerialize = False
    if SERIALIZE_BEST_TABLE:
        if tot_rewards > bestScoreLastSerialization:
            doSerialize = True
    else:
        if episode + 1 % SERIALIZE_TABLE_EVERY == 0:
            doSerialize = True

    if doSerialize:
        rf.serializeTable(qTableFileStr)
        bestScoreLastSerialization = tot_rewards

    # Decaying is being done every episode if episode number is within decaying range
    if END_EPSILON_DECAYING >= episode >= START_EPSILON_DECAYING:
        epsilon -= epsilon_decay_value

rf.serializeTable(qTableFileStr)

env.close()
